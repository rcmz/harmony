import java.rmi.*;
import java.rmi.registry.*;
import java.rmi.server.*;
import java.util.Scanner;

public class Client {

    public static void main(String[] args) {
        try {
            Registry registry = LocateRegistry.getRegistry();
            ChatService_itf server = (ChatService_itf) registry.lookup("Harmony");

            UserInterfaceImpl instance = new UserInterfaceImpl();
            UserInterface client = (UserInterface) UnicastRemoteObject.exportObject(instance, 0);

            Scanner scanner = new Scanner(System.in);
            System.out.print("please choose a username : ");

            while (true) {
                instance.setUsername(scanner.nextLine());
                if (server.join(client)) {
                    break;
                } else {
                    System.out.print("username already taken, please choose another : ");
                }
            }

            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override
                public void run() {
                    try {
                        server.leave(client);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
            });

            while (true) {
                server.send(client, scanner.nextLine());
            }
        } catch (Exception e) {
            System.err.println("Error on client: " + e);
        }
    }
}
