import java.rmi.*;

public class UserInterfaceImpl implements UserInterface {
    private String username;

    public UserInterfaceImpl(){}
    @Override
    public String getUsername() throws RemoteException{
        return username;
    }

    @Override
    public void receive(String message) throws RemoteException{
        System.out.println(message);
    }

    @Override
    public void setUsername(String name) throws RemoteException{
        this.username = name;
    }
}
