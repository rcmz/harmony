import java.rmi.*;

public interface ChatService_itf extends Remote {
    public boolean join(UserInterface client) throws RemoteException;
	public void leave(UserInterface client) throws RemoteException;
    public void send(UserInterface client, String message) throws RemoteException;
}
