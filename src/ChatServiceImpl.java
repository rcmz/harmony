import java.io.*;
import java.rmi.*;
import java.util.*;

public class ChatServiceImpl implements ChatService_itf {
    List<UserInterface> clients;

    public ChatServiceImpl() {
        this.clients = new ArrayList<>();
        File log = new File("log.txt");
        try {
            log.createNewFile();
        } catch (IOException e) {
            System.out.println("Error when creating log file : "+e);
            e.printStackTrace();
        }
    }

    private void sendAll(String message) throws RemoteException {
        for (UserInterface client : clients) {
            client.receive(message);
        }
    }

    @Override
    public boolean join(UserInterface client) throws RemoteException {
        for (UserInterface otherClient : clients) {
            if (otherClient.getUsername().equals(client.getUsername())) {
                return false;
            }
        }

        clients.add(client);
        sendAll(client.getUsername() + " joined");
        getLog(client);
        return true;
    }

    @Override
    public void leave(UserInterface client) throws RemoteException {
        clients.remove(client);
        sendAll(client.getUsername() + " left");
    }

    @Override
    public void send(UserInterface client, String message) throws RemoteException {
        String contents = client.getUsername() + "> " + message;
        sendAll(contents);
        updateLog(contents);
    }

    private void getLog(UserInterface client) {
        try (FileReader fr = new FileReader("log.txt"); BufferedReader br = new BufferedReader(fr)) {
            String line;
            while ((line = br.readLine()) != null) {
                client.receive(line);
            }
        } catch (Exception e) {
            System.out.println("Error on server when reading on log" + e);
            e.printStackTrace();
        }
    }

    private void updateLog(String message) {
        try (FileWriter fw = new FileWriter("log.txt", true);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter out = new PrintWriter(bw)) {
            out.println(message);
        } catch (Exception e) {
            System.out.println("Error on server when writing to log" + e);
            e.printStackTrace();
        }
    }
}
