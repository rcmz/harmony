import java.rmi.*;

public interface UserInterface extends Remote {
    public String getUsername() throws RemoteException;
    public void receive(String message) throws RemoteException;
    public void setUsername(String name) throws RemoteException;
}
