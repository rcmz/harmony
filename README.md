# RMI Chat Application "Harmony"

Ennes Abdelhafid

Richermoz Antoine

## How to compile the application

To compile the application, we use the following script :

```bash
#!/bin/bash

#compile Interfaces
javac -d classes -classpath .:classes src/UserInterface.java    
javac -d classes -classpath .:classes src/ChatService_itf.java
jar cvf ./lib/ChatService_itf.jar ./classes/ChatService_itf.class
jar cvf ./lib/UserInterface.jar ./classes/UserInterface.class


#compile Remote Objects Implementations
javac -d classes -classpath .:classes src/UserInterfaceImpl.java
javac -d classes -classpath .:classes src/ChatServiceImpl.java
jar cvf ./lib/UserInterfaceImpl.jar ./classes/UserInterfaceImpl.class
jar cvf ./lib/ChatServiceImpl.jar ./classes/ChatServiceImpl.class



#Compilation of client/server with ONLY the libraries they need to know (Emulation of different machines)
#Compile server
javac -d classes -cp .:classes:lib/ChatService_itf.jar:lib/ChatServiceImpl.jar src/Server.java
#Compile client 
javac -d classes -cp .:classes:lib/UserInterface.jar:lib/UserInterfaceImpl.jar:lib/ChatService_itf.jar src/Client.java
```

## How to run the application

To run the app, you can put yourself at the root of the project and do :

```bash
export CLASSPATH=$PATH/lib:$PATH/classes
# To run the Server
java Server
# To run a Client
java Client
```

With `$PATH` being the absolute path to the root of the project.

## How it works

Currently, our application supports 1 chatroom, where any number of users can log in and exchange messages. Each user is prompted to enter his username when connecting, and sees all the previously sent messages upon log-in, if any.

The server will export an instance of `ChatService_itf` to be called remotely, while each client will export an instance of `UserInterface` to do remote method calls on the server.

For example, when sending a message, each client will call `ChatService_itf.send(UserInterface client, String message)`, via a remote method invocation on the server, the parameter client being the exported `UserInterface` remote object.